/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m03.uf6.p1.grup07.controlador;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import m03.uf6.p1.grup07.vista.RenderizadorCeldas;
import m03.uf6.p1.grup07.vista.ModeloDeTablaSimple;

/**
 *
 * @author jmartin
 */
public class SelectorMetges extends javax.swing.JFrame {

    /**
     * Creates new form SelectorJugadores
     */
    public SelectorMetges() {
        initComponents();
        cbEquipos.removeAllItems();
        TableModel modelBuit = new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
            },
            new String [] {
                "nombre", "dorsal", "edad"
            }
        );
        
        tabJugadores.setModel(modelBuit);
        RenderizadorCeldas renderizador = new RenderizadorCeldas();
        for (int i = 0; i < tabJugadores.getColumnCount(); i++) {
            tabJugadores.getColumnModel().getColumn(i).setCellRenderer(renderizador);
        }
        
        try{
            ResultSet resultat = consultaBBDD("SELECT nombre FROM equipos");
            cbEquipos.addItem(null);
            while(resultat.next()){
                cbEquipos.addItem(resultat.getString("nombre"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    private ResultSet consultaBBDD(String consultaSQL) throws SQLException{
        
        Connection con = null;
        Statement sentencia = null;
        ResultSet resultat = null;
        try{
            con = GestorConnexio.getConnection();
            sentencia = con.createStatement();
            sentencia.executeQuery(consultaSQL);
            resultat = sentencia.getResultSet();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultat;
    }
    
    private void actualitzaTaula(JTable taula, TableModel model){
        taula.setModel(model);
        RenderizadorCeldas renderizador = new RenderizadorCeldas();
        for (int i = 0; i < taula.getColumnCount(); i++) {
            taula.getColumnModel().getColumn(i).setCellRenderer(renderizador);
        }
        // Nom mostrarem la columna id a la taula, però la necessitem per fer el update
        // si editem un camp
        TableColumnModel tcm = taula.getColumnModel();
        tcm.removeColumn(tcm.getColumn(0));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbEquipos = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabJugadores = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        cbEquipos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEquiposActionPerformed(evt);
            }
        });

        jLabel1.setText("Escoja un equipo");

        tabJugadores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabJugadores);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbEquipos, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEquipos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbEquiposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEquiposActionPerformed
        if(cbEquipos.getSelectedIndex() < 0){
            // Nada que hacer
        }else{
            String equipo = (String)(cbEquipos.getSelectedItem());
            try{
                ResultSet resultat = consultaBBDD(
                        //"SELECT jugadores.nombre, jugadores.dorsal, jugadores.edad"
                        "SELECT jugadores.id, jugadores.nombre, jugadores.dorsal, jugadores.edad "
                                + " FROM jugadores "
                                + "JOIN equipos ON jugadores.id_equipo = equipos.id "
                                + "WHERE equipos.nombre LIKE '" + equipo + "'");
                TableModel model = new ModeloDeTablaSimple(resultat);
                actualitzaTaula(tabJugadores, model);
                // Afegim un listener per salvar els canvis a la BBDD en modificar
                // el valor d'un camp de la taula
                tabJugadores.getModel().addTableModelListener(new TableModelListener() {
                    @Override
                    public void tableChanged(TableModelEvent e) {
                        actualitzaTaulaJugadors(e);
                    }
                });
                
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_cbEquiposActionPerformed

    private void actualitzaTaulaJugadors(TableModelEvent e){
        int fila = e.getFirstRow();
        PreparedStatement sentencia = null;
        Connection con = null;
        TableModel dades = tabJugadores.getModel();
        try {
            con = GestorConnexio.getConnection();
            String consulta = "UPDATE jugadores SET nombre = ?, dorsal = ?, edad = ? WHERE id = ?";
            sentencia = con.prepareStatement(consulta);
            sentencia.setString(1, (String)(dades.getValueAt(fila, 1)));
            sentencia.setInt(2, Integer.parseInt(dades.getValueAt(fila, 2).toString()));
            sentencia.setInt(3, Integer.parseInt(dades.getValueAt(fila, 3).toString()));
            sentencia.setInt(4, Integer.parseInt(dades.getValueAt(fila, 0).toString()));
            sentencia.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SelectorMetges.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SelectorMetges.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SelectorMetges.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SelectorMetges.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelectorMetges.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SelectorMetges().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cbEquipos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabJugadores;
    // End of variables declaration//GEN-END:variables
}
